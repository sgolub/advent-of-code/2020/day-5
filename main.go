package main

import (
	"fmt"
	"sort"

	"gitlab.com/sgolub/advent-of-code/base/v2/days"
)

const (
	maxRows = 128
	maxCols = 8
)

type myDay struct {
	data map[string]string
}

func (d myDay) SetData(key, value string) {
	d.data[key] = value
}

// Year will return the AOC Year
func (d myDay) Year() int {
	return 2020
}

// Day will return the day number for this puzzle
func (d myDay) Day() int {
	return 5
}

// GetData will load the data and parse it from disk
func (d myDay) GetData(useSampleData bool) []string {
	result := make([]string, 0)
	for k, v := range days.Control().LoadData(d, useSampleData) {
		if k == "passes" {
			for _, i := range v.([]interface{}) {
				result = append(result, i.(string))
			}
		}
	}
	return result
}

func getSeatCoords(boardingPass string) (int, int) {
	minY := 0
	maxY := 127
	minX := 0
	maxX := 7
	for _, dir := range boardingPass {
		y := (minY + maxY) / 2
		x := (minX + maxX) / 2
		switch dir {
		case 'F':
			maxY = y
		case 'B':
			minY = y
		case 'R':
			minX = x
		case 'L':
			maxX = x
		}
	}
	return maxY, maxX
}

func calcSeatId(row, col int) int {
	return (row * 8) + col
}

// Solution1 is the solution to the first part of the puzzle
func (d myDay) Solution1(useSample bool) string {
	data := d.GetData(useSample)
	defer days.NewTimer("D5P1")
	maxSeatId := 0
	for _, pass := range data {
		row, col := getSeatCoords(pass)
		seatId := calcSeatId(row, col)
		if seatId > maxSeatId {
			maxSeatId = seatId
		}
	}
	return fmt.Sprint(maxSeatId)
}

// Solution2 is the solution to the second part of the puzzle
func (d myDay) Solution2(useSample bool) string {
	defer days.NewTimer("D5P2")
	data := d.GetData(useSample)
	ids := []int{}
	for _, pass := range data {
		row, col := getSeatCoords(pass)
		ids = append(ids, calcSeatId(row, col))
	}
	sort.Ints(ids)
	for i, id := range ids {
		if i == 0 {
			continue
		}
		if id != ids[i-1]+1 {
			return fmt.Sprint(id - 1)
		}
	}
	return "failed to find id"
}

// GetDay will return this module's day. Used by the plugin loader.
func GetDay() days.Day {
	return myDay{
		data: map[string]string{},
	}
}
